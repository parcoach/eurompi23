cmake_minimum_required(VERSION 3.18)

project(tutorial-parcoach-eurompi23)

enable_language(C)

# Make it explicit which components we require from MPI
find_package(MPI REQUIRED C)
message(STATUS "Using mpicc: ${MPI_C_COMPILER}")

# The default 'COMPONENTS' looked for is Analysis, populating the PARCOACH_BIN
# and PARCOACHCC_BIN variables.
# In order to instrument a target we need to ask for the instrumentation library.
find_package(Parcoach REQUIRED COMPONENTS Instrumentation_C)

message(STATUS "Parcoach version ${Parcoach_VERSION} found in ${Parcoach_DIR}.")

set(DEMO_COLLECTIVES
  MPIexample
  mismatch_barrier
  )

# If using guix you will need to "export OMPI_MCA_plm_rsh_agent=`which false`" to run
# the tests.
foreach(PROG ${DEMO_COLLECTIVES})
  add_executable(${PROG} ${PROG}.c)
  target_link_libraries(${PROG} MPI::MPI_C)
  parcoach_coll_c_instrument(${PROG})
endforeach()

set(DEMO_RMA
  ll_get_get_inwindow
  )

foreach(PROG ${DEMO_RMA})
  add_executable(${PROG} ${PROG}.c)
  target_link_libraries(${PROG} MPI::MPI_C)
  parcoach_rma_c_instrument(${PROG})
endforeach()
