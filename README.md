# EuroMPI 2023 - PARCOACH tutorial

## Getting started

Start by cloning this repository and go to the folder containing this readme.


```bash
git clone https://gitlab.inria.fr/parcoach/eurompi23.git
cd eurompi23
```

### Docker

The easiest is by far to use the docker image for this repository, which contains everything you need to run PARCOACH and MPI.
One way is to call docker directly, and tell it to mount the current folder in the docker container:

```bash
docker run --rm -v `pwd`:/home/ubuntu/sources -it registry.gitlab.inria.fr/parcoach/eurompi23:2.4.2 bash
```

If you have docker compose, another way is to start a shell with it:
```bash
docker compose run --rm shell
```

### Guix

PARCOACH is packaged in the [guix-hpc](https://gitlab.inria.fr/guix-hpc/guix-hpc) channel; make sure you have this channel enabled (see [here](https://gitlab.inria.fr/guix-hpc/guix-hpc#how-does-it-work)), then you can use the manifest from this repository to get a working shell:

```bash
guix shell -m manifest.scm
```

The NAS Parallel Benchmarks are included in the docker image, you will need to get them locally if you want to follow that part of the tutorial:
```bash
wget https://www.nas.nasa.gov/assets/npb/NPB3.4.2.tar.gz
tar -xf NPB3.4.2.tar.gz
```

In order to run OpenMPI within a Guix shell you will also need to set the following variable once your shell is started:
```bash
export OMPI_MCA_plm_rsh_agent=`which false`
```

### On site USB keys

If you have a linux x86 computer but can't/don't want to use docker, we have usb keys going around: they contain an `env-eurompi23` folder that you can copy anywhere on your computer (eg: in your home).
It's 3GB and contains a few things:
  - `activate.sh`, when using bash, you can `source activate.sh` to activate a standalone environment containing everything you need for the tutorial (parcoach, clang, openmpi, cmake, etc)
  - the `gnu` folder: you can safely ignore what it contains, it's all the binaries and libraies, you shouldn't have to go in there manually.
  - the `NPB3.4.2` folder: contains the NAS Parallel Benchmark that will be used during the tutorial.

### Manual installation

We don't recommend this because docker/guix are just simpler/faster, but if you feel advanturous you can get the following dependencies to follow the tutorial; you need at least:
  - CMake
  - clang 15
  - OpenMPI
  - [PARCOACH 2.4.2](https://gitlab.inria.fr/api/v4/projects/12320/packages/generic/parcoach/2.4.2/parcoach-2.4.2-static-Linux.tar.gz)
  - [NAS Parallel Benchmarks](https://www.nas.nasa.gov/assets/npb/NPB3.4.2.tar.gz) (for the example)

If you are using an ubuntu/debian system, you can checkout the `Dockerfile` to see the packages installed!

## Hands on examples

The examples are divided in 3 parts:
  - First of all we will run parcoach manually on a simple MPI program, we will try to understand the differents steps performed by the tool, as well as the additional steps to run the instrumented binary.
  This example is described in details in [example-manual](./example-manual).
  - Then we will look into a CMake example, where we leverage CMake's `find_package` to find PARCOACH and use its helper to analyze/instrument CMake targets.
  This example is described in details in [example-cmake](./example-cmake).
  - Finally we will look into an existing make-based example: the IS (Integer Sort) kernel from the NAS Parallel Benchmarks.
  This example is described in details in [example-nas](./example-nas).
