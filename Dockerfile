FROM --platform=linux/amd64 ubuntu:lunar

ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]

RUN apt-get update
RUN apt-get install -yqq --no-install-recommends \
  binutils \
  binutils-dev \
  build-essential \
  ca-certificates \
  cmake \
  clang-15 \
  flang-15 \
  libflang-15-dev \
  git \
  g++ \
  gcc \
  gnupg \
  ninja-build \
  libopenmpi-dev \
  openssh-client \
  patch \
  sudo \
  unzip \
  vim \
  wget

# Set clang/clang++ as the default compiler.
# It's technically not necessary, but it allows for simpler cmake invocations.
RUN update-alternatives --install /usr/bin/cc cc /usr/bin/clang-15 100
RUN update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-15 100

RUN apt-get autoremove -yqq

ARG PARCOACH_VERSION=2.4.2
RUN wget https://gitlab.inria.fr/api/v4/projects/12320/packages/generic/parcoach/${PARCOACH_VERSION}/parcoach-${PARCOACH_VERSION}-static-Linux.tar.gz -O /tmp/parcoach.tgz
RUN tar -C /usr --strip-components=1 -xzf /tmp/parcoach.tgz

# Don't do this at home
RUN echo 'ubuntu ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER ubuntu
ENV SHELL=/bin/bash
WORKDIR /home/ubuntu

# Get the NAS Parallel Benchmark
RUN wget https://www.nas.nasa.gov/assets/npb/NPB3.4.2.tar.gz -O /tmp/nas.tgz
RUN tar -xzf /tmp/nas.tgz
# We can use the default make.def, we'll override OMPI_CC.
RUN cp NPB3.4.2/NPB3.4-MPI/config/make.def.template NPB3.4.2/NPB3.4-MPI/config/make.def
