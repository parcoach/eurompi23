# Solution to the exercise

The modified version of the makefile is in [MakefileInstr](./MakefileInstr).
Basically we need to change two things:
  - how the `is.o` object is generated, to instrument it
  - link PARCOACH's dynamic library to the final executable

The first point can be taken care of by changing the `OMPI_CC` variable when running make:
```bash
OMPI_CC="parcoachcc -instrum-inter --args clang-15" make is CLASS=S CC=clang-15
```

However `parcoachcc` needs to be aware *explicitly* about the final object file, therefore the following lines:

```make
is.o:             is.c  npbparams.h
```

Have been changed to this (notice the `-o is.o`):
```make
is.o:             is.c  npbparams.h
	${CCOMPILE} $< -o is.o
```

The second point can be taken care of by adding a library to the linker execution; this is the original linker step:
```make
${PROGRAM}: config ${OBJS}
	${CLINK} ${CLINKFLAGS} -o ${PROGRAM} ${OBJS} ${CMPI_LIB}
```

And this is one way to link the dynamic library into the executable:
```make
PARCOACH_LIB = -lParcoachCollDynamic_MPI_C

${PROGRAM}: config ${OBJS}
	${CLINK} ${CLINKFLAGS} -o ${PROGRAM} ${OBJS} ${CMPI_LIB} ${PARCOACH_LIB} ${LDFLAGS}
```

Note: the `${LDFLAGS}` is not necessary within the docker image, but only if you used the development environment from the USB key.

After this you can run the program and observe that there are no actual errors in NPB and that the reported warnings were false positives:
```bash
mpiexec -n 2 ./bin/is.S.x
```
